/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.odoo.ws;

import java.io.Serializable;

/**
 *
 * @author christopher
 */
public class Partner implements Serializable{
    
    private String name;
    private String street;
    private String phone;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getStreet() {
        return street;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    
    public Partner(){
        
    }
    
    public Partner(String name, String street, String phone) {
        this.name = name;
        this.street = street;
        this.phone = phone;
    }
    
    
}
