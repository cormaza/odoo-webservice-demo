/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.odoo.ws;

import java.util.ArrayList;
import java.util.Date;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author christopher
 */
@WebService(serviceName = "GetData")
public class GetData {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "getPartners")
    public ArrayList<Partner> getPartners(@WebParam(name = "referenceDate") Date referenceDates) {
        ArrayList<Partner> list = new ArrayList<Partner>();
        for (int i = 0; i < 10; i++) {
            Partner currentPartner = new Partner("Partner " + i, "Street " + i, "+59398016058" + i);
            list.add(currentPartner);
        }
        return list;
    }
}
